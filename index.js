import express, { json } from "express"                                                  // library export 
import firstRouter from "./src/routes/firstRouter.js"                          // for file export fileName.js is required or for starting with ./
import traineesRouter from "./src/routes/traineesRouter.js"
import schoolsRouter from "./src/routes/schoolsRouter.js"
import { connectToMongodb } from "./src/connectToDb/connectToMongodb.js"

import { studentRouter } from "./src/routes/studentRouter.js"
import { blogsRouter } from "./src/routes/blogsRouter.js"
import { classroomRouter } from "./src/routes/classroomRouter.js"
import { departmentRouter } from "./src/routes/departmentRouter.js"
import { contactRouter } from "./src/routes/contactRouter.js"
import bcrypt from 'bcrypt';
import { userRouter } from "./src/routes/userRouter.js"
import { productRouter } from "./src/routes/productRouter.js"
import { reviewRouter } from "./src/routes/reviewRouter.js"
import  jwt from "jsonwebtoken"
import { config } from "dotenv"
config()

console.log(process.env.NAME);

let expressApp=express()
expressApp.use(json()) 
expressApp.use(express.static('./public'))
                                                                          //middleware
// expressApp.use((req,res,next)=>{
//     console.log("i am application middleware")
//     next()
// })

connectToMongodb()                                                        //sudo service mongod start

expressApp.listen(8000,()=>{
    console.log("App is listening at port 8000")

})


expressApp.use('/bike',firstRouter)
expressApp.use('/trainees',traineesRouter)
expressApp.use('/schools',schoolsRouter)
expressApp.use('/students',studentRouter)
expressApp.use('/blogs',blogsRouter)
expressApp.use('/classroom',classroomRouter)
expressApp.use('/departments',departmentRouter)
expressApp.use('/contacts',contactRouter)
expressApp.use('/users',userRouter)
expressApp.use('/products',productRouter)
expressApp.use('/reviews',reviewRouter)






// let password="xyz%7"
// // let hashPassword=await bcrypt.hash(password,10)
// let hashPassword="$2b$10$1rWbqFdqnYgrezCCl.cfI.b8y1O3UGdyzAXQAfqBw6Szntt7M0lZW"
// // console.log(hashPassword);

// let isPasswordMatch=await bcrypt.compare(password,hashPassword)
// console.log(isPasswordMatch)

let infoObj={
    name:"Sushant",
    age:23,
    // _id:"2323xx9sdsd"
}

let secretKey="dw11"

let expiryDate={
    expiresIn:"365d",
}



// let token=jwt.sign(infoObj,secretKey,expiryDate)
// console.log(token)
let token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiU3VzaGFudCIsImFnZSI6MjMsImlhdCI6MTcwNjk0MjE4OCwiZXhwIjoxNzM4NDc4MTg4fQ.uOGJiTC-lxJHdgsDPRWoCgrdr9WIdAdj21cnajv0SZk"
try {
    let inObj=jwt.verify(token,"dw11")
    console.log(inObj)
} catch (error) {
    console.log(error.message);
}



























