
import { Schema } from "mongoose";

export let contactSchema=Schema({
    name:{
        type:String,
        required:true,
    },
    phoneNumber:{
        type:String,
        required:[true,"phone number field is required"],

    },
    email:{
        type:String,
        required:[true,"email field is required"]
    }
}
)






