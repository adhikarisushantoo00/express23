import { model } from "mongoose";
import { studentSchema } from "./studentSchema.js";
import { teacherSchema } from "./teacherSchema.js";
import { blogsSchema } from "./blogsSchema.js";
import { classroomSchema } from "./classroomSchema.js";
import { departmentSchema } from "./departmentSchema.js";
import { contactSchema } from "./contactSchema.js";
import { userSchema } from "./userSchema.js";
import { productSchema } from "./productSchema.js";
import { reviewSchema } from "./reviewSchema.js";

export let Student=model("Student",studentSchema)
export let Teacher=model("Teacher",teacherSchema)
export let Blog=model("Blog",blogsSchema)
export let Classroom=model("Classroom",classroomSchema)
export let Department=model("Department",departmentSchema)
export let Contact=model("Contact",contactSchema)
export let User=model("User",userSchema)
export let Product=model("Product",productSchema)
export let Review=model("Review",reviewSchema)






//model(arg1,arg2,..)        arg1=> saved in database






