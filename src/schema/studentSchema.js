import { Schema } from "mongoose";

export let studentSchema=Schema({
    name:{
        type:String,
        required:[true,"name field is required"],
        // lowercase:true,
        // uppercase:true,                                 //manipulation in mongoose
        //  trim:true,
         minLength:[3,"name must be at least 3 characters long"],
         maxLength:[30,"name must be at most 30 characters long"],
         validate:(value)=>{
            let onlyAlphabet=/^[a-zA-Z]+$/.test(value)
            if (onlyAlphabet){
                
            } 
            else {
                throw new Error("name must contain only alphabet")
                
            }
         }
    },
    phoneNumber:{
        type:Number,
        required:[true,"phoneNumber field is required"],
        trim:true,
        validate:(value)=> {
            let strPhoneNumber=String(value)
            if (strPhoneNumber.length===10&&(strPhoneNumber.startsWith("97")||strPhoneNumber.startsWith("98"))){
                
            } else {
                throw new Error("Np phone numbers must be ten digits starting with 97 or 98")
                
            }
        }

    },
    password:{
        type:String,
        required:[true,"password field is required"],
        validate:(value)=>{
            let isPasswordValid=/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()-_+=])[a-zA-Z0-9!@#$%^&*()-_+=]{8,15}$/
            .test(value)
            if (!isPasswordValid) {
                throw new Error("password  must contain 1 number,one lowercase,one uppercase,one symbol,min 8characters and max 15 characters long")
                
            } else {
                
            }
        }
    },
    roll:{
        type:Number,
        required:[true,"roll field is required"],
        min:[50,"roll must be greater than 50"],
        max:[100,"roll must be less than or equal to 100"],
    },
    spouseName:{
        type:String,
        required:[true,"spouseName field is required"]
    },
    email:{
        type:String,
        required:[true,"spouseName field is required"],
        unique:true,
        validate:(value)=>{
            let isValidEmail=/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value)
            if (!isValidEmail){
                throw new Error("email must be valid")
            }
            else {

            }
        }
    },
    gender:{
        type:String,
        default:"male",
        required:[true,"gender filed is required"],
        validate:(value)=>{
            if(value==="male"||value=="female"||value==="other"){  
            }
            else{
                throw new Error("Gender may either be male,female or other")
            }

        }
    },
    DOB:{
        type:Date,
        required:[true,"DOB field is required"]
    },
    location:{
        address:{
            type:String,
            required:[true,"address field is required"],
        },
        exactLocation:{
            type:String,
            required:[true,"exact location is required"],
        },
    },
    favTeacher:[
        {
            type:String,
        }
    ],
    favSubject: [
        {
        bookName:{
            type:String,
        },
        bookAuthor:{
            type:String,
        },
    }],
 })





