
import { Schema } from "mongoose";



export let userSchema=Schema({
    profileImage:{
        type:String,
        required:true,
    },
    name:{
        type:String,
        required: true,
    },
    address:{
        type:String,
        required:true,

    },
    phoneNumber:{
        type:String,
        required:true,
    },
    email:{
        type:String,
        required:true,
        unique:true

    },
    password:{
        type:String,
        required:true,
    }
})