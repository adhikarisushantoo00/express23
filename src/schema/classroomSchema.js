//classRoom=> , name, numberofBench, hasTv

import { Schema } from "mongoose";

export let classroomSchema=Schema({
    name:{
        type:Number,
        required:[true,"name field is required"],
        unique:true,

    },
    numberOfBench:{
        type:Number,
        required:[true,"number of benches field is required"]

    },
    hasTv:{
        type:Boolean,
        required:[true,"hasTv field is mandatory"]

    },
}
)



