import { Router } from "express";
import { createClassroom, 
         deleteClassroom, 
         getClassroom,
         getSpecificClassroom,
         updateClassroom,
        }               from "../controller/classroomController.js";

export let classroomRouter=Router()

classroomRouter
.route("/")
.post(createClassroom)
.get(getClassroom)


classroomRouter
.route("/:id")
.get(getSpecificClassroom)
.patch(updateClassroom)
.delete(deleteClassroom)











