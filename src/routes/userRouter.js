
import { Router } from "express";
import { createUser, deleteUser, getSpecificUser, getUser, updateUser } from "../controller/userController.js";


export let userRouter=Router()

userRouter
.route('/')
.post(createUser)
.get(getUser)


userRouter
.route('/:id')
.get(getSpecificUser)

.patch(updateUser)

.delete(deleteUser)