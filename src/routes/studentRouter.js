
import { Router } from "express";
import { createStudent, 
         deleteStudent, 
         getSpecificStudent, 
         getStudent, 
         updateStudent,
        } from "../controller/studentController.js";


export let studentRouter=Router()

studentRouter
.route('/')
.post(createStudent)
.get(getStudent)


studentRouter
.route('/:id')
.get(getSpecificStudent)

.patch(updateStudent)

.delete(deleteStudent)





