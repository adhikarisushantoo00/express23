import { Router } from "express"

 let firstRouter=Router()

firstRouter
.route("/")
.post((req,res,next)=>{
    let data=req.body
    res.json(data)                                         //send back to postman
    console.log(data);                                    //data sent from postman
})

// .get((req,res,next)=>{
//     console.log("I am middleware 1st")
//     next()
// },
// (req,res,next)=>{
//     console.log("I am middleware 2nd")

// })

.get((req,res,next)=>{
    console.log("I am middleware 1")
    let error=new Error("my error")
    // console.log(typeof(error))                 //object
    next(error)
    },
    (err,req,res,next)=>{
        console.log(err.message)
        console.log("I am  error middleware")
        next()
    },
    (req,res,next)=>{
        console.log("I am   middleware 2")
    }

)

.patch((
    req,res,next)=>{
    let data=req.body 
    res.send(data)
    console.log(data)
})



export default firstRouter;                 //used when ,only one variable is being exported



