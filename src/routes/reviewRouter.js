import { Router } from "express";
import { createReview, deleteReview, getReview, getSpecificReview, updateReview } from "../controller/reviewController.js";

export let reviewRouter=Router()

reviewRouter
.route('/')
.post(createReview)
.get(getReview)





reviewRouter
.route('/:id')
.get(getSpecificReview)

.patch(updateReview)

.delete(deleteReview)