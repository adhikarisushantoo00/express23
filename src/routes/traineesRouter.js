import { Router } from "express"

let traineesRouter=Router()


traineesRouter
.route("/")                                                    //dynamic route params -> use,,/ :id              :represents a dynamic route
.post((req,res,next)=>{ 
    
     
    res.json({
        success:true,
        msg:"trainees created successfully",
    })

})

traineesRouter
.route("/:id/system/:name")                                                     //dynamic route params -> use,,/ :id              :represents a dynamic route
.post((req,res,next)=>{ 
    console.log(req.query)
    console.log(req.body)
    console.log(req.params)
    res.json({
        success:true,
        msg:"trainees created successfully",
    })
})

.get((req,res,next)=>{                                                         
    console.log(req.params)
    console.log(req.query)
    res.json({
        success:true,
        msg:"trainees read successfully",
    })
})


.patch((req,res,next)=>{
    res.json({
        success:true,
        msg:"trainees updated successfully",

    })
})




.delete((req,res,next)=>{
    res.json({
        success:true,
        msg:"trainees deleted successfully",
    })
})

export default traineesRouter;















