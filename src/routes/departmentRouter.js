import { Router } from "express";
import { createDepartment, deleteDepartment, getDepartment, getSpecificDepartment, updateDepartment } from "../controller/departmentController.js";

export let departmentRouter=Router()
departmentRouter
.route("/")
.post(createDepartment)
.get(getDepartment)

departmentRouter
.route("/:id")
.get(getSpecificDepartment)
.patch(updateDepartment)
.delete(deleteDepartment)





