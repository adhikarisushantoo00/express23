import { Router } from "express";

import { createProduct, deleteProduct, getProduct, getSpecificProduct, updateProduct } from "../controller/productController.js";

export let productRouter=Router()

productRouter
.route('/')
.post(createProduct)
.get(getProduct)




productRouter
.route('/:id')
.get(getSpecificProduct)

.patch(updateProduct)

.delete(deleteProduct)