import { Router } from "express";
import { Blog } from "../schema/model.js";
import { get } from "mongoose";

export let blogsRouter=Router()

blogsRouter
.route('/')
.post(async(req,res,next)=>{
    let data=req.body
    try {
        let addBlog=await Blog.create(data)
        res.json({
            success:true,
            message:"blog added successfully",
            _blog:addBlog
        }) 
    }
    catch(error){
        res.json({
            success:false,
            message:error.message,
        })
    }
})

.get(async(req,res,next)=>{
    let getAllBlogs=await Blog.find({})
    res.json({
        success:true,
        message:"blogs read successfully",
        _blogs:getAllBlogs,
    })

})


blogsRouter
.route('/:id')
.get(async(req,res,next)=>{
    let id=req.params.id
    try {
        let getSingleBlog=await Blog.findById(id)
        res.json({
            success:true,
            message:"blog read successfully",
            _blog:getSingleBlog,
        })  
    } 
    catch (error){
        res.json({
            success:false,
            message:error.message,
        })
    }
})

.patch(async(req,res,next)=>{
    let id=req.params.id
    let data=req.body
    try {
        let updatedBlog=await Blog.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:"blog updated successfully",
            _blog:updatedBlog,
        })
    }
    catch(error){
        res.json({
            success:false,
            message:error.message
        })    
    }
})

.delete(async(req,res,next)=>{
    let id=req.params.id
    try {
        let removeBlog=await Blog.findOneAndDelete(id)     //findByIdAndDelete
        res.json({
            success:true,
            message:"blog deleted successfully",
        })
    }
    catch(error){
        res.json({
            success:false,
            message:error.message
        })    
    }
})



