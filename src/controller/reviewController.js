import { Review } from "../schema/model.js"
export let createReview=async(req,res,next)=>{
    let data=req.body
    try {
        let result=await Review.create(data)
        res.json({
            success:true,
            message:"Review created successfully",
            result:result,
        })   
    }

    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let getReview=async(req,res,next)=>{
    try {
        let result =await Review.find({}).populate("productId").populate("userId")
        res.json({
            success:true,
            message:"students read successfully",
            result:result,
        })
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let getSpecificReview=async(req,res,next)=>{
    let id=req.params.id
    try {
        let result=await Review.findById(id)
        res.json({
            success:true,
            message:"student read successfully",
            result:result,
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let updateReview=async(req,res,next)=>{
    let id=req.params.id
    let data=req.body
    try {
        let result=await Review.findByIdAndUpdate(id,data,{new:true})   //if {new:false} (default),it gives old data
        res.json({
            success:true,
            message:"student updated successfully",
            result:result
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}


export let deleteReview=async(req,res,next)=>{
    let id=req.params.id
    try {
        let result=await Review.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"student deleted successfully",
            result:result,
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}