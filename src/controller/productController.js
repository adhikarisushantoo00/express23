import { Product } from "../schema/model.js"
export let createProduct=async(req,res,next)=>{
    let data=req.body
    try {
        let result=await Product.create(data)
        res.json({
            success:true,
            message:"Product created successfully",
            result:result,
        })   
    }

    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let getProduct=async(req,res,next)=>{
    try {
        let result =await Product.find({})
        res.json({
            success:true,
            message:"students read successfully",
            result:result,
        })
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let getSpecificProduct=async(req,res,next)=>{
    let id=req.params.id
    try {
        let result=await Product.findById(id)
        res.json({
            success:true,
            message:"student read successfully",
            result:result,
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let updateProduct=async(req,res,next)=>{
    let id=req.params.id
    let data=req.body
    try {
        let result=await Product.findByIdAndUpdate(id,data,{new:true})   //if {new:false} (default),it gives old data
        res.json({
            success:true,
            message:"student updated successfully",
            result:result
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}


export let deleteProduct=async(req,res,next)=>{
    let id=req.params.id
    try {
        let result=await Product.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"student deleted successfully",
            result:result,
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}
