import { Classroom } from "../schema/model.js"

export let createClassroom=async(req,res,next)=>{
    let data=req.body
    try {
        let result=await Classroom.create(data)
        res.json({
            success:true,
            message:"classroom created successfully",
            result:result,
        })
    }
    catch (error){
        res.json({
            success:false,
            message:error.message,
        })
        
    }
}

export let getClassroom=async(req,res,next)=>{
    try {
        let result=await Classroom.find({})
        res.json({
            success:true,
            message:"classrooms read successfully",
            result:result,
        })
    }
    catch (error){
        res.json({
            success:false,
            message:error.message,
        })
        
    }
}

export let getSpecificClassroom=async(req,res,next)=>{
    let id=req.params.id
    try {
        let result=await Classroom.findById(id)
        res.json({
            success:true,
            message:"classroom read successfully",
            result:result,
        })
    }
    catch (error){
        res.json({
            success:false,
            message:error.message,
        })
        
    }
}

export let updateClassroom=async(req,res,next)=>{
    let id=req.params.id
    let data=req.body
    try {
        let result=await Classroom.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:"classroom updated successfully",
            result:result,
        })
    }
    catch (error){
        res.json({
            success:false,
            message:error.message,
        })
        
    }
}


export let deleteClassroom=async(req,res,next)=>{
    let id=req.params.id
    try {
        let result=await Classroom.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"classroom deleted successfully",
            result:result,
        })
    }
    catch (error){
        res.json({
            success:false,
            message:error.message,
        })
        
    }
}