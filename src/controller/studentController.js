import { Student } from "../schema/model.js"

export let createStudent=async(req,res,next)=>{
    let data=req.body
    try {
        let result=await Student.create(data)
        res.json({
            success:true,
            message:"Student created successfully",
            result:result,
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let getStudent=async(req,res,next)=>{
    try {
        let result =await Student.find({}).limit("5").skip("2")
        res.json({
            success:true,
            message:"students read successfully",
            result:result,
        })
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let getSpecificStudent=async(req,res,next)=>{
    let id=req.params.id
    try {
        let result=await Student.findById(id)
        res.json({
            success:true,
            message:"student read successfully",
            result:result,
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let updateStudent=async(req,res,next)=>{
    let id=req.params.id
    let data=req.body
    try {
        let result=await Student.findByIdAndUpdate(id,data,{new:true})   //if {new:false} (default),it gives old data
        res.json({
            success:true,
            message:"student updated successfully",
            result:result
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}


export let deleteStudent=async(req,res,next)=>{
    let id=req.params.id
    try {
        let result=await Student.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"student deleted successfully",
            result:result,
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}













