import { Contact } from "../schema/model.js"

export let createContact=async(req,res,next)=>{
    let data=req.body
    try {
        let result=await Contact.create(data)
        res.json({
            success:true,
            message:"Contact created successfully",
            result:result,
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let getContact=async(req,res,next)=>{
    let limit=req.params.limit
    let page=req.params.page
    try {
        // let result =await Contact.find({}).skip("1").limit("1")
        //limit=2 & page=4  => skip-6        //  limit=2 & page=3

        let result =await Contact.find({}).skip((page-1)*limit).limit(limit)     
        res.json({
            success:true,
            message:"students read successfully",
            result:result,
        })
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let getSpecificContact=async(req,res,next)=>{
    let id=req.params.id
    try {
        let result=await Contact.findById(id)
        res.json({
            success:true,
            message:"student read successfully",
            result:result,
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let updateContact=async(req,res,next)=>{
    let id=req.params.id
    let data=req.body
    try {
        let result=await Contact.findByIdAndUpdate(id,data,{new:true})   //if {new:false} (default),it gives old data
        res.json({
            success:true,
            message:"student updated successfully",
            result:result
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}


export let deleteContact=async(req,res,next)=>{
    let id=req.params.id
    try {
        let result=await Contact.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"student deleted successfully",
            result:result,
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

