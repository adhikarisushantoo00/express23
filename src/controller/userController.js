import { User } from "../schema/model.js"
import bcrypt from "bcrypt"

export let createUser=async(req,res,next)=>{
    let data=req.body
    let password=data.password
    let hashPassword=await bcrypt.hash(password,10)          //saltRounds => no of hashing rounds  (optimal-10)
    data.password=hashPassword
    try {
        let result=await User.create(data)
        res.json({
            success:true,
            message:"User created successfully",
            result:result,
        })   
    }

    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let getUser=async(req,res,next)=>{
    try {
        let result =await User.find({})
        res.json({
            success:true,
            message:"students read successfully",
            result:result,
        })
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let getSpecificUser=async(req,res,next)=>{
    let id=req.params.id
    try {
        let result=await User.findById(id)
        res.json({
            success:true,
            message:"student read successfully",
            result:result,
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}

export let updateUser=async(req,res,next)=>{
    let id=req.params.id
    let data=req.body
    try {
        let result=await User.findByIdAndUpdate(id,data,{new:true})   //if {new:false} (default),it gives old data
        res.json({
            success:true,
            message:"student updated successfully",
            result:result
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}


export let deleteUser=async(req,res,next)=>{
    let id=req.params.id
    try {
        let result=await User.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"student deleted successfully",
            result:result,
        })   
    }
    catch (error) {
            res.json({
                success:false,
                message:error.message,
            })
    }
}
