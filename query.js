                //Searching//
 
 
 //.find({})
        // .find({name:"Nodejs"})
        //.find({name:"Sushant",roll:17,email:"cj1700@gmail.com"})





        // ***//while searching even if type of roll is NUMBER ,IF "17"(STRING) is passed it still produces result.(focus is only  on value not type)***
                   //same for boolean and other types true or "true"                     
        //Number Searching(Exact Searching)



        // .find({roll:{$gte:50}})      //gt
        //  .find({roll:{$lte:20}})      //lt
        //  .find({roll:{$ne:50}})          //model.find({roll:50})  for equals to
        //  .find({roll:{$in:[20,25,30]}})
        //   .find({roll:{$gte:20,$lte:25}})           //define range 20-25



        // String Searching(Exact Searching&(Regex Searching=>Non-exact Searching))



        // .find({name:{$in:["sushant","Nitan"]}})
        // .find({name:"nitan"})                  //exact searching  
        // .find({name:/nitan/})                  //non-exact searching         [accepts "AA*nitanBB*..."]
        // .find({name:/nitan/i})                 //
        // .find({name:/ni/})                      // substring "ni"
        // .find({name:/ni/i})                     // case insensitive
        // .find({name:/^ni/})                     // startsWith "ni"
        // .find({name:/^ni/i})                    // startsWith & case insensitive 
        // .find({name:/ni$/})                     // endsWith "ni"

        //select


        // .find({}).select("name email -_id")
        // .find({}).select("-password -_id")

        //find has control over the object whereas select has control over the object property.
        //in select use either all - or use all +, don't use both except for _id.

        // Sorting

        // .find({}).sort("name")
        // .find({}).sort("-name")
        // .find({}).sort("age -name")
        // .find({}).sort("name age")
        // .find({}).sort("name -age")

        //Pagination
        // skip("3")  -> skips first three results(?)
        //  precedence : find -> sort -> select -> skip ->limit